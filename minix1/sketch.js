function setup() {
    // put setup code here
   
   //Making canvas as wide as window 
    createCanvas(windowWidth,windowHeight);
    // refresh frame every second
    frameRate(11);
   
   }
   
   function draw() {
     // put drawing code here


    //making background gradually darker shades of blue
    let col = map (mouseX,0,windowWidth,0,255);
    background (30,30,col)
     
     // making grass
     fill (0,200,0);
     rect(0, 500, windowWidth);

     //house
     fill(random(0, 255), random(0, 255), random(0, 255));
     rect(200, 250, 400,250);

     //roof
     fill(random(0, 255), random(0, 255), random(0, 255));
     triangle(200,250,400,100,600,250)

     //sun random placement and random yellow color 
   
     let sun = map (mouseX,0,windowWidth,0,255)
     noStroke();
     fill(254, sun, 0);
     circle(mouseX, 67,64)


     
    
//door random size and red color 
     fill(random(100, 255), random(0, 0), random(0, 0));
    rect(350,300,random(50,110),200)

    
    stroke(0);
    strokeWeight(3);
    line(360, 400, 380, 400);

    //making windows 
    strokeWeight(1)
     noFill();
     square(270, 320, 55);
     //lodret
     line(298, 320, 298, 375);
     //vandret
     line(270, 347, 325, 347);


     noFill();
     square(470, 320, 55);
     //lodret
     line(498, 320, 498, 375);
     //vandret
     line(470, 347, 526, 347);


     



     //flowers
fill(250,250,0)
     circle(100,550,12)

     fill(200,200,200)
     ellipse(100, 534, 15, 20)
     ellipse(100, 567, 15, 20)
     ellipse(84, 551, 20, 15)
     ellipse(116, 551, 20, 15)

fill(230,230,0)
     circle(200,550,12)
     fill(100,200,200)
     ellipse(200, 534, 15, 20)
     ellipse(200, 567, 15, 20)
     ellipse(184, 551, 20, 15)
     ellipse(216, 551, 20, 15)


fill(250,250,0)
     circle(300,550,12)

     fill(200,200,200)
     ellipse(300, 534, 15, 20)
     ellipse(300, 567, 15, 20)
     ellipse(284, 551, 20, 15)
     ellipse(316, 551, 20, 15)

fill(230,230,0)
     circle(400,550,12)
     fill(100,200,200)
     ellipse(400, 534, 15, 20)
     ellipse(400, 567, 15, 20)
     ellipse(384, 551, 20, 15)
     ellipse(416, 551, 20, 15)


fill(250,250,0)
     circle(500,550,12)
     fill(200,200,200)
     ellipse(500, 534, 15, 20)
     ellipse(500, 567, 15, 20)
     ellipse(484, 551, 20, 15)
     ellipse(516, 551, 20, 15)

fill(230,230,0)
     circle(600,550,12)
     fill(100,200,200)
     ellipse(600, 534, 15, 20)
     ellipse(600, 567, 15, 20)
     ellipse(584, 551, 20, 15)
     ellipse(616, 551, 20, 15)


fill(250,250,0)
     circle(700,550,12)
     fill(200,200,200)
     ellipse(700, 534, 15, 20)
     ellipse(700, 567, 15, 20)
     ellipse(684, 551, 20, 15)
     ellipse(716, 551, 20, 15)

  
     }


   
function mousePressed(){
    background (200,200,0);
     circle(200,200,300)


      // Draw eyes
    fill(255); // White fill for the eyes
    ellipse(140, 160, 60, 60); // Left eye
    ellipse(260, 160, 60, 60); // Right eye

    // Draw pupils
    fill(0); // Black fill for the pupils
    ellipse(140, 160, 20, 20); // Left pupil
    ellipse(260, 160, 20, 20); // Right pupil

    // Draw mouth (smile)
    noFill(); // No fill for the mouth
    strokeWeight(4); // Increase stroke weight for the mouth
    arc(200, 240, 200, 120, 0, PI); // Arc for the smile


   
   

}
